package dev.resttemplate.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TokenResponse {
    private String success;
    private Long code;
    private String message;
    private String data;
}
