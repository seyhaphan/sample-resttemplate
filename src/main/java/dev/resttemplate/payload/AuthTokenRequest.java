package dev.resttemplate.payload;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AuthTokenRequest {
    private String username;
    private String password;
}
