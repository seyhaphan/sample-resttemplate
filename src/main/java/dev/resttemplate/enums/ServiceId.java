package dev.resttemplate.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ServiceId {

    // AUTH
    USER_LOGIN("AUTH-0100"),
    USERS("USER-0100")
    ;

    private final String value;

    private ServiceId(String value) {
        this.value = value;
    }

    @JsonCreator
    public static ServiceId fromValue(String value) {
        for(ServiceId my: ServiceId.values()) {
            if(my.value.equals(value)) {
                return my;
            }
        }

        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public String getLabel() {
        String label = "";

        return label;
    }
}
