package dev.resttemplate.service;

import dev.resttemplate.component.MyClient;
import dev.resttemplate.enums.ServiceId;
import dev.resttemplate.payload.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class MyService {

    @Autowired
    HttpServletRequest request;

    public void test(){
        try {
          Object  resp =  MyClient.sendRequest(request, null, ServiceId.USER_LOGIN);

            System.err.println(resp.toString());
        } catch (Throwable e){
            e.printStackTrace();
        }

    }

    public List<UserResponse> getUser() {

        List<UserResponse> userResponses = null;

        try {
            userResponses   = (List<UserResponse>) MyClient.sendRequest(request, null, ServiceId.USERS);

        } catch (Throwable e){
            e.printStackTrace();
        }

        return userResponses;
    }
}
