package dev.resttemplate.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Base64;


@Slf4j
public class HttpUtils {

    public static String getFullUrl(String baseUrl ,String path,String params){
        return getFullUrl(baseUrl,path)+"/"+params ;
    }

    public static String getFullUrl(String baseUrl,String path){
        return baseUrl + path;
    }

    public static HttpHeaders getHeader ()  {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        return header;
    }

    public static HttpHeaders getBearerHeader (String token)  {
        HttpHeaders header = getHeader ();
        header.setBearerAuth(token);
        return header;
    }

    public static HttpHeaders getBasicHeader (String username,String password)  {
        HttpHeaders header = getHeader ();
        String token = Base64.getEncoder().encodeToString((username+":"+password).getBytes(StandardCharsets.UTF_8));
        header.setBasicAuth(token);
        return header;
    }
    public static HttpHeaders setMediaType(){
        HttpHeaders header = getHeader ();
        return header;
    }

    public static HttpEntity getEntity(Object entity , HttpHeaders header){
        return new HttpEntity(entity,header);
    }
    public static HttpEntity getEntityNoHeader(Object entity){
        return new HttpEntity(entity);
    }

    public static HttpEntity getEntityNoEntity(HttpHeaders header){
        return new HttpEntity(HttpEntity.EMPTY,header);
    }

    public static String sendRequest(String url, HttpMethod httpMethod, HttpEntity entity){
        log.info("============REQUEST========================");
        log.info(url);
        log.info(entity.toString());
        ResponseEntity<String> responseEntity = new RestTemplate().exchange(url,httpMethod,entity,String.class);
        return responseEntity.getBody();
    }

}
