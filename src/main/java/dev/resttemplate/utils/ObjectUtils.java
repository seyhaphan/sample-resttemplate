package dev.resttemplate.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.Map;
import java.util.stream.Collectors;

public class ObjectUtils extends org.apache.commons.lang3.ObjectUtils {
    static private ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public static String writeValueAsString(Object value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    // Convert POJO to Map
    public static String convertPojoToMap(Object pojo)
    {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(pojo, new TypeReference<Map<String, Object>>() {});
        return map.entrySet().stream().map(x -> {
            return x.getKey() + "=" + x.getValue();
        }).collect(Collectors.joining("&"));

        // Convert Map to POJO
        // Foo anotherFoo = mapper.convertValue(map, Foo.class);
    }

    public static <T> T readValue(String str, TypeReference<T> tr) {
        try {
            return mapper.readValue(str, tr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
