package dev.resttemplate.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "credential")
@Getter
@Setter
public class LmsConfig {
    private String apiUrl;
    private String username;
    private String password;
}
