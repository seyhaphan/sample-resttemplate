package dev.resttemplate.component;


import com.fasterxml.jackson.core.type.TypeReference;
import dev.resttemplate.enums.ServiceId;
import dev.resttemplate.payload.AuthTokenRequest;
import dev.resttemplate.payload.TokenResponse;
import dev.resttemplate.payload.UserResponse;
import dev.resttemplate.properties.LmsConfig;
import dev.resttemplate.utils.HttpUtils;
import dev.resttemplate.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class MyClient extends HttpUtils {

    private static  String BASE_URL;
    private static  String USERNAME;
    private static  String PASSWORD;

    private static Map<ServiceId, String> resourceMap;


    /**
     * Inject Configuration
     * @param lmsConfig
     */
    @Autowired
    public MyClient(LmsConfig lmsConfig){
        BASE_URL = lmsConfig.getApiUrl();
        USERNAME = lmsConfig.getUsername();
        PASSWORD = lmsConfig.getPassword();

    }

    static {

        resourceMap = new HashMap<>();

        // put api url here with service id
        resourceMap.put(ServiceId.USER_LOGIN, "/api/v1/login");
        resourceMap.put(ServiceId.USERS, "/api/user");

    }

    /**
     *
     * @param req if you want to get header from request
     * @param payload
     * @param serviceId
     * @return
     * @throws Exception
     */
    public static Object sendRequest(HttpServletRequest req,Object payload,ServiceId serviceId) throws Exception {

        String apiUrl = BASE_URL;

        if (serviceId == null) {
            throw new RuntimeException(MessageFormat.format("{0} is not implemented", payload.getClass().getSimpleName()));
        }

        if (!resourceMap.containsKey(serviceId)) {
            throw new RuntimeException(MessageFormat.format("{0} is not implemented", serviceId.getValue()));
        }

        apiUrl = apiUrl + resourceMap.get(serviceId);

        String respData = null;

        TokenResponse tokenResponse = getAuthToken();

        HttpEntity<MultiValueMap<String, String>> httpEntity = getEntity(payload,getBearerHeader(tokenResponse.getData()));

        log.info("[Url] [" + apiUrl + "]");

        switch (serviceId) {

            case USERS:
                respData = sendRequest(apiUrl,HttpMethod.GET,httpEntity);
                break;
            default:
                throw new RuntimeException(MessageFormat.format("{0} service is not implemented", serviceId.getValue()));
        }

        log.info("[Request] [" + httpEntity.toString() + "]");
        if(respData != null) {

            log.info("[Response] " + respData);
            return parseData(respData, serviceId);
        }

        return null;
    }


    /**
     * GetAuthorization
     * @return TokenResponse
     */
    public static TokenResponse getAuthToken(){

        AuthTokenRequest authTokenRequest = AuthTokenRequest.builder()
                .username(USERNAME)
                .password(PASSWORD)
                .build();

        HttpEntity<MultiValueMap<String, String>> httpEntity = getEntity(authTokenRequest, getHeader());

        String fullUrl = getFullUrl(BASE_URL, resourceMap.get(ServiceId.USER_LOGIN));

        TokenResponse tokenResponse = ObjectUtils.readValue(sendRequest(fullUrl, HttpMethod.POST, httpEntity), new TypeReference<TokenResponse>() {});

        return tokenResponse;
    }


    /**
     * Parse String data to response class
     * @param responseData
     * @param serviceId
     * @return Object
     */
    public static Object parseData(String responseData, ServiceId serviceId) {

        switch(serviceId) {

            case USERS:
                List<UserResponse> users = ObjectUtils.readValue(responseData, new TypeReference<List<UserResponse>>() {});
                return users;
//            case ORGANIZATION:
//                DepartmentResponse[] departmentResponses = GsonUtils.arrayList(responseData,DepartmentResponse[].class);
//                return departmentResponses;
            default:
                throw new RuntimeException(MessageFormat.format("{0} service is not implemented", serviceId.getValue()));
        }
    }


}
